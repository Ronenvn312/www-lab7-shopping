package com.example.lab7shoppingproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Lab7ShoppingProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(Lab7ShoppingProjectApplication.class, args);
    }

}
