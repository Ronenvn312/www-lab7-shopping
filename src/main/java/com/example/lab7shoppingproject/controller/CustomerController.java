package com.example.lab7shoppingproject.controller;

import com.example.lab7shoppingproject.constant.PageDefault;
import com.example.lab7shoppingproject.models.Customer;
import com.example.lab7shoppingproject.models.Product;
import com.example.lab7shoppingproject.services.CustomerService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;


    @GetMapping("/paging")
    public String getPaging(Model model, @RequestParam("page") Optional<Integer> page,
                                               @RequestParam("size") Optional<Integer> size ) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Customer> customersPage = customerService.findPaginated(currentPage-1,pageSize,"name","asc");
        model.addAttribute("customersPage", customersPage);
        return "client/customer/customer-paging";
    }
    @PostMapping("/add")
    public String addCustomer(@Valid Customer customer, Model model, HttpSession session) {
        customerService.addCustomer(customer);
        return "redirect:/customers/paging";
    }

    @PostMapping("/update/{id}")
    public String updateCustomer(@PathVariable("id") long id, @Valid Customer customer, BindingResult result, Model model) {

        customerService.updateCustomer(customer,id);
        return "redirect:/customers/paging";
    }
    @GetMapping("/delete/{id}")
    public String deleteCustomer(@PathVariable("id") long id) {
        customerService.deleteCustomer(id);
        return "redirect:/customers/paging";
    }
    @GetMapping("/updatePage/{id}")
    public ModelAndView updatePage(Model model, @PathVariable("id") long id) {
        ModelAndView modelAndView = new ModelAndView();
        Customer customer = customerService.findCustomerById(id);
        modelAndView.addObject(customer);
        modelAndView.setViewName("client/customer/customer-update-page");
        return modelAndView;
    }
    @GetMapping("/addPage")
    public ModelAndView addPage(Model model){
        ModelAndView modelAndView = new ModelAndView();
        Customer customer = new Customer();
        modelAndView.addObject("customer", customer);
        modelAndView.setViewName("client/customer/customer-add-paging");
        return modelAndView;
    }
}
