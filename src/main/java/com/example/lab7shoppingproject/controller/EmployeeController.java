package com.example.lab7shoppingproject.controller;

import com.example.lab7shoppingproject.constant.PageDefault;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/employees")
public class EmployeeController {
    @GetMapping("")
    public String getPaging(@RequestParam(defaultValue = PageDefault.NO) int no,
                            @RequestParam(defaultValue = PageDefault.LIMIT) int limit ) {
        return "client/employee/employee-paging";
    }
}
