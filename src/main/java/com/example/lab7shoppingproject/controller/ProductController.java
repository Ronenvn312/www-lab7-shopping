package com.example.lab7shoppingproject.controller;

import com.example.lab7shoppingproject.constant.PageDefault;
import com.example.lab7shoppingproject.models.Product;
import com.example.lab7shoppingproject.services.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productServices;

    @GetMapping("/page")
    public ResponseEntity<?> getALlProductPage(@RequestParam(defaultValue = PageDefault.NO) int no,
                                               @RequestParam(defaultValue = PageDefault.LIMIT) int limit ) {
        return ResponseEntity.ok(this.productServices.findPaginated(no, limit, "product_id", "asc"));
    }

    @GetMapping("/paging")
    public String showProductListPaging(Model model,
                                        @RequestParam("page") Optional<Integer> page,
                                        @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(3);
        Page<Product> productPage = productServices.findPaginated(currentPage -1 , pageSize, "name","asc");

        model.addAttribute("productPage", productPage);
        int totalPages = productPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "client/product/product-paging";
    }
    @PostMapping("/add")
    public String addProduct(@Valid Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "client/product/add-product";
        }
        productServices.addProduct(product);
        return "redirect:/products/paging";
    }
    @PostMapping("/update/{id}")
    public String updateProduct(@PathVariable("id") long id,
                                @Valid Product product,
                                BindingResult result, Model model) {
        if (result.hasErrors()) {
            product.setProduct_id(id);
            return "client/product/update-product";
        }
        productServices.updateProduct(product, id);
        return "redirect:/products/paging";
    }
    @GetMapping("/delete/{id}")
    public String deleteProduct(@PathVariable("id") long id) {

        productServices.deleteProduct(id);
        return "redirect:/products/paging";
    }
    @GetMapping("/add-product-form")
    public ModelAndView ShowAddProductForm(Model model) {
        ModelAndView modelAndView= new ModelAndView();
        Product product = new Product();
        modelAndView.addObject("product", product);
        modelAndView.setViewName("client/product/add-product");
        return modelAndView;
    }
    @GetMapping("/update-product-form/{id}")
    public ModelAndView ShowUpdateForm(Model model,
                                       @PathVariable("id") long id
                                       ) {

        ModelAndView modelAndView= new ModelAndView();
        Product product = productServices.findProductById(id);
        modelAndView.addObject("product", product);
        modelAndView.setViewName("client/product/update-product");
        return modelAndView;
    }

    @GetMapping("/product-detail/{id}")
    public ModelAndView showProductDetail(Model model, @PathVariable("id") long id){
        ModelAndView modelAndView = new ModelAndView();
        Product product = productServices.findProductById(id);
        modelAndView.addObject("product", product);
        modelAndView.setViewName("client/product-detail");
        return modelAndView;
    }
}
