package com.example.lab7shoppingproject.controller;

import com.example.lab7shoppingproject.constant.PageDefault;
import com.example.lab7shoppingproject.models.Order;
import com.example.lab7shoppingproject.models.OrderDetail;
import com.example.lab7shoppingproject.services.OrderDetailService;
import com.example.lab7shoppingproject.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/order-details")
public class OrderDetailController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/order")
    public String getPaging(Model model, @RequestParam("id") long orderId  ) {

        Order order = orderService.findOrderById(orderId);
//        List<OrderDetail> orderDetails = order.getOrderDetails();
        model.addAttribute("order", order);
        return "client/order-detail/order-detail-paging";
    }
}
