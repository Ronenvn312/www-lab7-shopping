package com.example.lab7shoppingproject.controller;

import com.example.lab7shoppingproject.constant.LocalDateTypeAdapter;
import com.example.lab7shoppingproject.models.*;
import com.example.lab7shoppingproject.services.CustomerService;
import com.example.lab7shoppingproject.services.EmployeeService;
import com.example.lab7shoppingproject.services.ProductService;
import com.example.lab7shoppingproject.services.impl.CartManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("")
public class HomeController {
    @Autowired
    private ProductService productServices;
    @Autowired
    private CartManager cartManager;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private CustomerService customerService;
    private final Cart cart = new Cart();
    @GetMapping("")
    public String showHome() {
        return "client/index";
    }
    @GetMapping("/home")
    public String showForHome(HttpServletRequest request, HttpSession session, @CookieValue(value = "cartCount",defaultValue = "5") String shoppingCart, Model model ) {

//        model.addAttribute("cartCountCookie", cartCount);
        String resultCookie;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            resultCookie = Arrays.stream(cookies)
                    .map(c -> c.getName() + "=" + c.getValue()).collect(Collectors.joining(", "));
            System.out.println(resultCookie);
        }
//
//        if (cart1 != null) {
//            model.addAttribute("cookieCart", cart1);
//        } else {
//            cart1 = cartManager.getCart(session);
//            model.addAttribute("cookieCart", cart1);
//        }
        return "client/index";
    }

    @GetMapping("/shop")
    public String showForShop(HttpSession session, Model model,
                              @RequestParam("page") Optional<Integer> page,
                              @RequestParam("size") Optional<Integer> size){
        int currentPage = page.orElse(1);
        int pageSize = page.orElse(10);
        Page<Product> productPage = productServices.findPaginated(currentPage -1 , pageSize, "name","asc");

        Cart cart1 = cartManager.getCart(session);
        model.addAttribute("productPage", productPage);
        int totalPages = productPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1,totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "client/shop";
    }
    @PostMapping("/add-to-cart")
    public String addToCart(HttpServletResponse response, HttpSession session, @RequestParam("id") long id,
                            @RequestParam(value = "qty", required = false, defaultValue = "1") int qty)  {
//        Gson gson =  new GsonBuilder()
//                .registerTypeAdapter(LocalDate.class, new LocalDateTypeAdapter())
//                .create();
        // Fetch the product by productId from your database or some service
        Product product = productServices.findProductById(id);
        Cart cart1 = cartManager.getCart(session);
        cart1.addItem(product, qty);
        System.out.println(cart1);
        //add cookie for cartCount
        for (CartItem c: cart1.getItems()
             ) {
            try {
                Cookie cookieProduct = new Cookie("cart" + c.getProduct().getProduct_id() + ":", c.getProduct().getProduct_id()+"");
                cookieProduct.setMaxAge(3600);
                response.addCookie(cookieProduct);
            } catch (Exception e) {
                System.err.println(e.getMessage());
            }

        }
        return "redirect:/shop";
    }

    @RequestMapping("/remove")
    public String remove(HttpSession session, @RequestParam("id") Product product){
        Cart cart = cartManager.getCart(session);
        cart.removeItem(product);
        return "redirect:/shop";
    }

    @RequestMapping("/update")
    public String update(HttpSession session, @RequestParam("id") Product product, @RequestParam("qty") int qty){
        Cart cart = cartManager.getCart(session);
        cart.updateItem(product, qty);
        return "redirect:/shop";
    }

    @PostMapping("/update-in-cart")
    public String updateInCart(HttpSession session, @RequestParam("id") Product product, @RequestParam("qty") int qty){
        Cart cart = cartManager.getCart(session);
        cart.updateItem(product, qty);
        return "redirect:/cart";
    }
    @RequestMapping("/remove-in-cart")
    public String removeInCart(HttpSession session, @RequestParam("id") Product product){
        Cart cart = cartManager.getCart(session);
        cart.removeItem(product);
        return "redirect:/cart";
    }
    @GetMapping("/about")
    public String showForAbout() {
        return "client/about";
    }
    @GetMapping("/services")
    public String showForServices() {
        return "client/services";
    }
    @GetMapping("/blog")
    public String showForBlog() {
        return "client/blog";
    }
     @GetMapping("/contact")
    public String showForContact() {
        return "client/contact";
    }
    @GetMapping("/cart")
    public String showForCart(HttpSession session, Model model) {
        Cart cart1 = cartManager.getCart(session);
        if (cart1 != null) {
            model.addAttribute("listCart", cart1);
        }

        return "client/cart";
    }
    @GetMapping("/login")
    public ModelAndView loginPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }
}
