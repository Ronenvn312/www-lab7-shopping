package com.example.lab7shoppingproject.controller;

import com.example.lab7shoppingproject.constant.PageDefault;
import com.example.lab7shoppingproject.models.*;
import com.example.lab7shoppingproject.services.*;
import com.example.lab7shoppingproject.services.impl.CartManager;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private CustomerService customerService;
    @Autowired
    private CartManager cartManager;
    @Autowired
    private ProductService productService;;
    @Autowired
    private OrderDetailService orderDetailService;


    @GetMapping("")
    public String getPaging(Model model, @RequestParam("page") Optional<Integer> page,
                            @RequestParam("size") Optional<Integer> size ) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Page<Order> orderPage = orderService.findPaginated(currentPage-1 , pageSize, "orderDate", "asc");
        model.addAttribute("orderPage", orderPage);


        return "client/order/order-paging";
    }
    @PostMapping("")
    public String addOrder(HttpSession session, Model model) {
        Order order = new Order();
        Cart cart = cartManager.getCart(session);
        //get value default add for order
        // when we use security then let's get it from auth content
        Employee employee = employeeService.findEmployeeById(1);
        Customer customer = customerService.findCustomerById(1);
        model.addAttribute("employee", employee);
        model.addAttribute("customer", customer);
        System.out.println(employee);
        System.out.println(customer);
        //
        List<CartItem> items = cart.getItems();
        List<OrderDetail> orderDetails = new ArrayList<>();
        order.setCustomer(customer);
        order.setEmployee(employee);

        order.setOrderDate(LocalDateTime.now());
        System.out.println(order);
        // Save Order for cus
        Order orderResult = orderService.addOrder(order);
        //get all item from cart to save order detail
        for (CartItem item: items
        ) {
            Product product = productService.findProductById(item.getProduct().getProduct_id());
            OrderDetail orderDetail = new OrderDetail(item.getQuantity(), product.getProductPrices().get(0).getPrice(),"note",orderResult, product);
            OrderDetail orderDetailResult = orderDetailService.addOrderDetail(orderDetail);
            orderDetails.add(orderDetailResult);
            //remove all items in cart
            cart.removeItem(item.getProduct());
        }
        // set order detail for order result
        order.setOrderDetails(orderDetails);
        // remove all items in session

        return "redirect:/orders";
    }
}
