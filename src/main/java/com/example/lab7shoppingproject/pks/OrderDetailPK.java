package com.example.lab7shoppingproject.pks;

import com.example.lab7shoppingproject.models.Order;
import com.example.lab7shoppingproject.models.Product;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter @Getter
public class OrderDetailPK implements Serializable {
    private Order order;
    private Product product;
}
