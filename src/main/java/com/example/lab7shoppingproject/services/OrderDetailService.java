package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.OrderDetail;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OrderDetailService {

    public Page<OrderDetail> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<OrderDetail> findAllPro();

    public OrderDetail addOrderDetail(OrderDetail orderDetail);

    public boolean updateOrderDetail(OrderDetail orderDetail, long orderDetailId);
    public boolean deleteOrderDetail(long orderDetailId);

    OrderDetail findOrderDetailById(long id);
}
