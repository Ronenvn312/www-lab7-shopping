package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.Order;
import org.springframework.data.domain.Page;

import java.util.List;

public interface OrderService {

    public Page<Order> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<Order> findAllPro();

    public Order addOrder(Order order);

    public boolean updateOrder(Order order, long orderId);
    public boolean deleteOrder(long orderId);

    Order findOrderById(long id);
}
