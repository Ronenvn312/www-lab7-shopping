package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.ProductPrice;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductPriceService {

    public Page<ProductPrice> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<ProductPrice> findAllPro();

    public boolean addProductPrice(ProductPrice productPrice);

    public boolean updateProductPrice(ProductPrice productPrice, long productPriceId);
    public boolean deleteProductPrice(long productPriceId);

    ProductPrice findProductPriceById(long id);
}
