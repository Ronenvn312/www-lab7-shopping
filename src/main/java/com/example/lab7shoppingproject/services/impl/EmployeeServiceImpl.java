package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.Employee;
import com.example.lab7shoppingproject.repository.EmployeeRepository;
import com.example.lab7shoppingproject.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Page<Employee> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return employeeRepository.findAll(pageable);
    }
    @Override
    public List<Employee> findAllPro() {
        return employeeRepository.findAll();
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if (employee != null){
            employeeRepository.save(employee);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateEmployee(Employee employee, long employeeId) {
        if (employeeRepository.existsById(employeeId)){
            employeeRepository.save(employee);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteEmployee(long employeeId) {
        if (employeeRepository.existsById(employeeId)){
            employeeRepository.deleteById(employeeId);
            return true;
        }
        return false;
    }

    @Override
    public Employee findEmployeeById(long id) {
        return employeeRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
