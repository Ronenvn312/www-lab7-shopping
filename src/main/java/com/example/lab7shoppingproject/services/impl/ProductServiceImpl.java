package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.Product;
import com.example.lab7shoppingproject.repository.ProductRepository;
import com.example.lab7shoppingproject.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Page<Product> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return productRepository.findAll(pageable);
    }
    @Override
    public List<Product> findAllPro() {
        return productRepository.findAll();
    }

    @Override
    public boolean addProduct(Product product) {
        if (product != null){
            productRepository.save(product);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateProduct(Product product, long productId) {
        if (productRepository.existsById(productId)){
            productRepository.save(product);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteProduct(long productId) {
        if (productRepository.existsById(productId)){
            productRepository.deleteById(productId);
            return true;
        }
        return false;
    }

    @Override
    public Product findProductById(long id) {
        return productRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
