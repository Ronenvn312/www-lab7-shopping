package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.Order;
import com.example.lab7shoppingproject.repository.OrderRepository;
import com.example.lab7shoppingproject.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Page<Order> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return orderRepository.findAll(pageable);
    }
    @Override
    public List<Order> findAllPro() {
        return orderRepository.findAll();
    }

    @Override
    public Order addOrder(Order order) {
        if (order != null){
            return orderRepository.save(order);
        }
        return null;
    }

    @Override
    public boolean updateOrder(Order order, long orderId) {
        if (orderRepository.existsById(orderId)){
            orderRepository.save(order);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteOrder(long orderId) {
        if (orderRepository.existsById(orderId)){
            orderRepository.deleteById(orderId);
            return true;
        }
        return false;
    }

    @Override
    public Order findOrderById(long id) {
        return orderRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
