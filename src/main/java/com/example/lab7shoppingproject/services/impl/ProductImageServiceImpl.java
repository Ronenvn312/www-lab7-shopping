package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.ProductImage;
import com.example.lab7shoppingproject.repository.ProductImageRepository;
import com.example.lab7shoppingproject.services.ProductImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductImageServiceImpl implements ProductImageService {

    @Autowired
    private ProductImageRepository productImageImageRepository;

    @Override
    public Page<ProductImage> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return productImageImageRepository.findAll(pageable);
    }
    @Override
    public List<ProductImage> findAllPro() {
        return productImageImageRepository.findAll();
    }

    @Override
    public boolean addProductImage(ProductImage productImageImage) {
        if (productImageImage != null){
            productImageImageRepository.save(productImageImage);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateProductImage(ProductImage productImageImage, long productImageImageId) {
        if (productImageImageRepository.existsById(productImageImageId)){
            productImageImageRepository.save(productImageImage);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteProductImage(long productImageImageId) {
        if (productImageImageRepository.existsById(productImageImageId)){
            productImageImageRepository.deleteById(productImageImageId);
            return true;
        }
        return false;
    }

    @Override
    public ProductImage findProductImageById(long id) {
        return productImageImageRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
