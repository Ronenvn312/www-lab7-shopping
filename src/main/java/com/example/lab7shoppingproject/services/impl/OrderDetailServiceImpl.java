package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.OrderDetail;
import com.example.lab7shoppingproject.repository.OrderDetailRepository;
import com.example.lab7shoppingproject.services.OrderDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderDetailServiceImpl implements OrderDetailService {

    @Autowired
    private OrderDetailRepository orderDetailRepository;

    @Override
    public Page<OrderDetail> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return orderDetailRepository.findAll(pageable);
    }
    @Override
    public List<OrderDetail> findAllPro() {
        return orderDetailRepository.findAll();
    }

    @Override
    public OrderDetail addOrderDetail(OrderDetail orderDetail) {
        if (orderDetail != null){
            return orderDetailRepository.save(orderDetail);
        }
        return null;
    }

    @Override
    public boolean updateOrderDetail(OrderDetail orderDetail, long orderDetailId) {
        if (orderDetailRepository.existsById(orderDetailId)){
            orderDetailRepository.save(orderDetail);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteOrderDetail(long orderDetailId) {
        if (orderDetailRepository.existsById(orderDetailId)){
            orderDetailRepository.deleteById(orderDetailId);
            return true;
        }
        return false;
    }

    @Override
    public OrderDetail findOrderDetailById(long id) {
        return orderDetailRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
