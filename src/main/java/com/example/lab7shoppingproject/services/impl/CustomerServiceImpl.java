package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.Customer;
import com.example.lab7shoppingproject.models.Customer;
import com.example.lab7shoppingproject.repository.CustomerRepository;
import com.example.lab7shoppingproject.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public Page<Customer> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return customerRepository.findAll(pageable);
    }
    @Override
    public List<Customer> findAllPro() {
        return customerRepository.findAll();
    }

    @Override
    public boolean addCustomer(Customer customer) {
        if (customer != null){
            customerRepository.save(customer);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateCustomer(Customer customer, long customerId) {
        if (customerRepository.existsById(customerId)){
            customerRepository.save(customer);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteCustomer(long customerId) {
        if (customerRepository.existsById(customerId)){
            customerRepository.deleteById(customerId);
            return true;
        }
        return false;
    }

    @Override
    public Customer findCustomerById(long id) {
        return customerRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
