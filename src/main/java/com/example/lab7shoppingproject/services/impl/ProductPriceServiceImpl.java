package com.example.lab7shoppingproject.services.impl;

import com.example.lab7shoppingproject.models.ProductPrice;
import com.example.lab7shoppingproject.repository.ProductPriceRepository;
import com.example.lab7shoppingproject.services.ProductPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductPriceServiceImpl implements ProductPriceService {
    @Autowired
    private ProductPriceRepository productPriceRepository;

    @Override
    public Page<ProductPrice> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection) {
        Sort sort = Sort.by(Sort.Direction.fromString(sortDirection), sortBy);
        Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
        return productPriceRepository.findAll(pageable);
    }
    @Override
    public List<ProductPrice> findAllPro() {
        return productPriceRepository.findAll();
    }

    @Override
    public boolean addProductPrice(ProductPrice productPrice) {
        if (productPrice != null){
            productPriceRepository.save(productPrice);
            return true;
        }
        return false;
    }

    @Override
    public boolean updateProductPrice(ProductPrice productPrice, long productPriceId) {
        if (productPriceRepository.existsById(productPriceId)){
            productPriceRepository.save(productPrice);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteProductPrice(long productPriceId) {
        if (productPriceRepository.existsById(productPriceId)){
            productPriceRepository.deleteById(productPriceId);
            return true;
        }
        return false;
    }

    @Override
    public ProductPrice findProductPriceById(long id) {
        return productPriceRepository.findById(id).orElseThrow(() -> new RuntimeException());
    }
}
