package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.Product;
import org.springframework.data.domain.*;

import java.util.List;


public interface ProductService {

    public Page<Product> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<Product> findAllPro();

    public boolean addProduct(Product product);

    public boolean updateProduct(Product product, long productId);
    public boolean deleteProduct(long productId);

    Product findProductById(long id);
}
