package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.ProductImage;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ProductImageService {
    public Page<ProductImage> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<ProductImage> findAllPro();

    public boolean addProductImage(ProductImage productPriceImage);

    public boolean updateProductImage(ProductImage productPriceImage, long productPriceImageId);
    public boolean deleteProductImage(long productPriceImageId);

    ProductImage findProductImageById(long id);
}
