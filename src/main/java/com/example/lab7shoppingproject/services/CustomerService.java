package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.Customer;
import org.springframework.data.domain.Page;

import java.util.List;

public interface CustomerService {

    public Page<Customer> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<Customer> findAllPro();

    public boolean addCustomer(Customer customer);

    public boolean updateCustomer(Customer customer, long customerId);
    public boolean deleteCustomer(long customerId);

    Customer findCustomerById(long id);
}
