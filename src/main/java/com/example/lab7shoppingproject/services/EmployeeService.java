package com.example.lab7shoppingproject.services;

import com.example.lab7shoppingproject.models.Employee;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EmployeeService {

    public Page<Employee> findPaginated(int pageNo, int pageSize, String sortBy, String sortDirection);

    public List<Employee> findAllPro();

    public boolean addEmployee(Employee employee);

    public boolean updateEmployee(Employee employee, long employeeId);
    public boolean deleteEmployee(long employeeId);

    Employee findEmployeeById(long id);
}
